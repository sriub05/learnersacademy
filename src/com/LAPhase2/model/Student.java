
package com.LAPhase2.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name ="STUDENT")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "STUDENT_ID")
	private int studentId;
	@Column(name = "STUDENT_NAME")
	private String studentName;
	@Column(name = "STUDENT_PHONE")
	private String studentPhone;
	@Column(name = "STUDENT_ADDRESS")
	private String studentAddress;
	@Column(name = "STUDENT_EMERGENCYCONTACT")
	private String studentEmergencycontact;
	public Student() {	
	}

	public Student(String studentName) {
		this.studentName = studentName;
	}
	
	public int getStudentId() {
		return studentId;
	}

	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	public String getStudentName() {
		return studentName;
	}


	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentPhone() {
		return studentPhone;
	}


	public void setStudentPhone(String studentPhone) {
		this.studentPhone = studentPhone;
	}
	
	public String getStudentAddress() {
		return studentAddress;
	}


	public void setStudentAddress(String studentAddress) {
		this.studentAddress = studentAddress;
	}
	
	public String getStudentEmergencycontact() {
		return studentEmergencycontact;
	}


	public void setStudentEmergencycontact(String studentEmergencycontact) {
		this.studentEmergencycontact = studentEmergencycontact;
	}
	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentName=" + studentName + ",studentPhone=" + studentPhone + ",studentAddress=" + studentAddress + 
				",studentEmergencycontact=" + studentEmergencycontact + "]";
	}

	
}
