package com.LAPhase2.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.LAPhase2.model.Teacher;
import com.LAPhase2.service.impl.TeacherServiceImplementation;
import com.LAPhase2.service.TeacherService;

@Path("/teacher")
public class TeacherController {
	

	private TeacherService service = new TeacherServiceImplementation();

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Teacher createStudent(Teacher teacher) {

		return service.createTeacher(teacher);
	}

	@GET
	@Path("/{teacherId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Teacher getTeacherById(@PathParam("teacherId") int teacherId) {

		return service.getTeacherById(teacherId);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Teacher> getAllTeachers() {

		return service.getAllTeachers();
	}

	@PATCH
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Teacher updateTeacher(Teacher teacher) {

		return service.updateTeacher(teacher);
	}

	@DELETE
	@Path("/{teacherId}")
	@Consumes(MediaType.APPLICATION_JSON)
	public void removeStudent(@PathParam("teacherId") int teacherId) {

		service.deleteTeacher(teacherId);

	}


}