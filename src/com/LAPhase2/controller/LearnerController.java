package com.LAPhase2.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;


@Path("/")
public class LearnerController {
	
	@GET
	public String Welcome() {
		return "*******Welcome to Learner's Academy*******";
	}
}