package com.LAPhase2.service;

import java.util.List;

import com.LAPhase2.model.Classes;

public interface ClassesService {

	public Classes createClasses(Classes classes);
	public Classes getClassesById(int classId);
	public List<Classes> getAllClasses();
	public Classes updateClasses (Classes classes);
	public void deleteClasses(int classId);
	
	
	
}