package com.LAPhase2.service.impl;

import java.util.List;

import com.LAPhase2.dao.impl.SubjectDaoImplementation;
import com.LAPhase2.model.Subject;
import com.LAPhase2.dao.SubjectDAO;
import com.LAPhase2.service.SubjectService;

public class SubjectServiceImplementation implements SubjectService {
	
	private SubjectDAO dao = new SubjectDaoImplementation();

	@Override
	public Subject createSubject(Subject subject) {

		return dao.createSubject(subject);
	}

	@Override
	public Subject getSubjectById(int subjectId) {
		return dao.getSubjectById(subjectId);
 
	}

	@Override
	public List<Subject> getAllSubject() {

		return dao.getAllSubject();
	}

	@Override
	public Subject updateSubject(Subject subject) {

		return dao.updateSubject(subject);
	}

	@Override
	public void deleteSubject(int subjectId) {

		dao.deleteSubject(subjectId);

	}

}