package com.LAPhase2.service.impl;

import java.util.List;

import com.LAPhase2.dao.impl.ClassesDaoImplementation;
import com.LAPhase2.model.Classes;
import com.LAPhase2.dao.ClassesDAO;
import com.LAPhase2.service.ClassesService;

public class ClassesServiceImplementation implements ClassesService {

	private ClassesDAO dao = new ClassesDaoImplementation();

	@Override
	public Classes createClasses(Classes classes) {

		return dao.createClasses(classes);

	}

	@Override
	public Classes getClassesById(int classId){

		return dao.getClassesById(classId);
		
	}

	@Override
	public List<Classes> getAllClasses() {

		return dao.getAllClasses();
	}

	@Override
	public Classes updateClasses(Classes classes) {

		return dao.updateClasses(classes);

	}

	@Override
	public void deleteClasses(int classId) {

		dao.deleteClasses(classId);

	}
}